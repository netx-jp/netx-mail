import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MySnackBar{

  static show(
      BuildContext buildContext,
      Widget icon,
      String msg)
  {
    ScaffoldMessenger.of(buildContext).showSnackBar(SnackBar(
      content: Row(
        children: [
          SizedBox(
            height: 20,
            width: 20,
            child: icon,
          ),
          SizedBox(width: 20),
          Text(msg,
            style: TextStyle(
                color: Colors.white
            ),
          )
        ],
      ),
      backgroundColor: Colors.blueAccent,
      duration: (icon == loadingIcon) ? Duration(minutes: 5) : Duration(seconds: 2),
    ));
  }

  static Widget loadingIcon = CircularProgressIndicator(
    strokeWidth: 2,
  );

  static Widget errorIcon = Icon(
    Icons.error,
    color: Colors.white,
  );

  static Widget warningIcon = Icon(
    Icons.warning_amber_outlined,
    color: Colors.white,
  );

  static Widget successIcon = Icon(
    Icons.check_circle_rounded,
    color: Colors.white,
  );

  static hide(BuildContext context){
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
  }

}