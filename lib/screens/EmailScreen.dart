import 'dart:io';
import 'dart:typed_data';
import 'package:enough_mail/mail_address.dart';
import 'package:enough_mail/message_builder.dart';
import 'package:enough_mail/mime_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:netxmail/models/Data.dart';
import 'package:netxmail/constants.dart';
import 'package:netxmail/models/UserInfo.dart';
import 'package:netxmail/screens/MailSender.dart';
import 'package:netxmail/utilities/MySnackBar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

class EmailScreen extends StatefulWidget {
  EmailScreen({
    Key key,
    this.mimeMessage,
    this.userInfo
  }) : super(key: key);

  final MimeMessage mimeMessage;
  final UserInfo userInfo;

  @override
  EmailScreenState createState() => EmailScreenState(
      mimeMessage: mimeMessage,
      userInfo: userInfo
  );

}

class EmailScreenState extends State<EmailScreen> {

  MimeMessage mimeMessage;
  UserInfo userInfo;
  EmailScreenState({Key key,this.mimeMessage,this.userInfo});

  @override
  void initState() {
    super.initState();
  }

  Future<bool> saveFile(BuildContext context,Uint8List uint8List, String fileName) async {

    Directory directory;

    try {
      if (Platform.isAndroid) {
        if (await requestPermission(Permission.storage)) {

          directory = await getExternalStorageDirectory();
          String newPath = "";
          print(directory);
          List<String> paths = directory.path.split("/");

          for (int x = 1; x < paths.length; x++) {
            String folder = paths[x];
            if (folder != "Android") {
              newPath += "/" + folder;
            } else {
              break;
            }
          }

          newPath = newPath + "/NetxMail";
          directory = Directory(newPath);

        } else {
          return false;
        }
      } else {
        if (await requestPermission(Permission.photos)) {
          directory = await getTemporaryDirectory();

        } else {
          return false;
        }
      }

      if (!await directory.exists()) {
        await directory.create(recursive: true);
      }

      if (await directory.exists()) {
        File file = new File('${directory.path}/$fileName');
        print("file path = ${file.path}");
        await file.writeAsBytes(uint8List);
        return true;
      }

      return false;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  void updateMimeMessage(MimeMessage mm){
    setState(() {
      mimeMessage = mm;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: mimeMessage == null ? Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.touch_app_outlined,
              color: Colors.red,
            ),
            Text(
              "Please tap any mail from left side!",
              style: TextStyle(
                color: Colors.red,
                fontSize: 20
              ),
            )
          ],
        ),
      ) : Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent,
          toolbarHeight: kDefaultToolbarHeight,
          title: Text("Message Details"),
          actions: [
            IconButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MailSender(
                    userInfo: userInfo,
                    data: Data(
                      messageBuilder: MessageBuilder.prepareReplyToMessage(
                        mimeMessage,
                        MailAddress('', userInfo.email),
                        replyAll: false,
                        quoteOriginalText: true,
                        replyToSimplifyReferences: true
                      ),
                      isNewMessage: false
                    ),
                  ))
                );
              },
              icon: Icon(Icons.reply)
            )
          ],
        ),
        body: Container(
          color: Colors.white,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(kDefaultPadding),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              mimeMessage.fromEmail,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 15
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              mimeMessage.decodeSubject().toString(),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 20
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              DateFormat().format(DateTime.parse(mimeMessage.decodeDate().toString())),
                              style: Theme.of(context).textTheme.caption,
                            ),
                            SizedBox(height: kDefaultPadding),
                            LayoutBuilder(
                              builder: (context, constraints) => SizedBox(
                                width: constraints.maxWidth > 850
                                    ? 800
                                    : constraints.maxWidth,
                                child: SingleChildScrollView(
                                  child: mimeMessage.decodeTextHtmlPart() == null ?
                                  Text(mimeMessage.decodeTextPlainPart() == null ?
                                  "Empty Body" : mimeMessage.decodeTextPlainPart()) :
                                  Html(
                                    data: mimeMessage.decodeTextHtmlPart(),
                                    padding: EdgeInsets.all(8.0),
                                    onLinkTap: (url) async {
                                      await canLaunch(url) ?
                                      await launch(url) :
                                      MySnackBar.show(
                                          context,
                                          MySnackBar.errorIcon,
                                        "Can't launch the URL!"
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: mimeMessage.hasAttachments() ? Container(
          height: 50,
          color: Colors.redAccent,
          padding: EdgeInsets.fromLTRB(10.0, 5.0, 0, 5.0),
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: mimeMessage.findContentInfo().length,
              itemBuilder: (context, index) {
                ContentInfo contentInfo = mimeMessage
                    .findContentInfo()[index];
                return InkWell(
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0),
                    margin: EdgeInsets.only(right: 5.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            child: Text(
                              contentInfo.fileName,
                              overflow: TextOverflow.ellipsis,
                            )
                        ),
                        Icon(
                          Icons.download,
                          color: Colors.grey,
                        )
                      ],
                    ),
                  ),
                  onTap: (){
                    MimePart mimePart = mimeMessage.getPart(contentInfo.fetchId);
                    Uint8List uint8List = mimePart.decodeContentBinary();

                    Fluttertoast.showToast(msg: "Downloading....");
                    saveFile(context,uint8List,contentInfo.fileName).then((value){
                      MySnackBar.hide(context);
                      if(value){
                        Fluttertoast.showToast(msg: "Completed.");
                      }else{
                        Fluttertoast.showToast(msg: "Something went wrong!");
                      }
                    }).catchError((err){
                      Fluttertoast.showToast(msg: "Something went wrong!");
                    });
                  },
                );
              }
          ),
        ) : null,
      )
    );
  }
}
