import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enough_mail/enough_mail.dart';
import 'package:enough_mail/message_builder.dart';
import 'package:enough_mail/smtp/smtp_client.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:multi_select_flutter/chip_display/multi_select_chip_display.dart';
import 'package:multi_select_flutter/dialog/mult_select_dialog.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';
import 'package:netxmail/constants.dart';
import 'package:netxmail/models/Data.dart';
import 'package:netxmail/models/EmailInfo.dart';
import 'package:netxmail/models/UserInfo.dart';
import 'package:netxmail/utilities/Alert.dart';
import 'package:netxmail/utilities/MySharedPreferences.dart';
import 'package:netxmail/utilities/MySnackBar.dart';

class MailSender extends StatefulWidget {
  MailSender({Key key, this.userInfo, this.data}) : super(key: key);

  final UserInfo userInfo;
  final Data data;

  @override
  MailSenderState createState() => MailSenderState(
      userInfo: userInfo,
      data: data
  );
}

class MailSenderState extends State<MailSender> {

  UserInfo userInfo;
  Data data;
  MailSenderState({Key key,this.userInfo, this.data});

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  List<EmailInfo> selectedCCEmailInfos = [];
  List<EmailInfo> selectedBCCEmailInfos = [];
  TextEditingController ccTextEditCtl = new TextEditingController();
  TextEditingController bccTextEditCtl = new TextEditingController();
  TextEditingController toTextEditCtl = new TextEditingController();
  TextEditingController subjectTextEditCtl = new TextEditingController();
  TextEditingController bodyTextEditCtl = new TextEditingController();

  List<File> attachmentInfos = [];
  AlertDialog alertDialog;

  HtmlEditorController htmlEditorController = HtmlEditorController();
  bool needToFreezeUi;
  MessageBuilder messageBuilder;

  @override
  void initState() {
    super.initState();
    needToFreezeUi = false;
    messageBuilder = data.messageBuilder;
    if(!data.isNewMessage){
      toTextEditCtl.text = data.messageBuilder.to.first.email;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AbsorbPointer(
      absorbing: needToFreezeUi,
      child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.red,
              title: Text("New Message"),
              actions: [
                IconButton(
                    onPressed: (){
                      onFileSelect(context);
                    },
                    icon: Icon(Icons.attach_file)
                ),
                IconButton(
                    onPressed: () async {
                      verifyInput(context).then((isVerified){
                        if(isVerified){
                          sendMail(context);
                        }
                      });
                    },
                    icon: Icon(Icons.send)
                )
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          top: 10,
                          left: 10
                      ),
                      child: Text("TO",
                          style: TextStyle(
                              fontSize: 17
                          )
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      decoration: kBoxDecorationStyle,
                      child: TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(left: 15.0),
                        ),
                        controller: toTextEditCtl,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 10,
                          left: 10
                      ),
                      child: Text("CC",
                          style: TextStyle(
                              fontSize: 17
                          )
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      decoration: kBoxDecorationStyle,
                      child: TextField(
                        controller: ccTextEditCtl,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(top: 15.0),
                            suffixIcon: IconButton(
                              icon: Icon(Icons.add),
                              onPressed: (){
                                bool isVerified = verifyBeforeAddEmail(
                                  context,
                                  ccTextEditCtl.text,
                                  true
                                );
                                if(isVerified){
                                  addEmailAddress(1);
                                }
                              },
                            ),
                            prefixIcon: IconButton(
                              icon: Icon(Icons.search),
                              onPressed: (){
                                MySharedPreferences.getStringValue("emailInfos").then((emailInfos){
                                  if(emailInfos == null){
                                    showMultiSelect(context,1,[]);
                                  }else {
                                    List<EmailInfo> emailInfoList = emailInfos
                                        .split(";")
                                        .map((e) => EmailInfo(emailAddress: e))
                                        .toList();
                                    showMultiSelect(context,1,emailInfoList);
                                  }
                                });
                              },
                            )
                        ),
                      ),
                    ),
                    MultiSelectChipDisplay(
                      items: selectedCCEmailInfos.map((e) => MultiSelectItem(
                          e, e.emailAddress
                      )).toList(),
                      height: 5.0,
                      chipWidth: 50,
                      textStyle: TextStyle(
                        fontSize: 10,
                      ),
                      onTap: (value) {
                        setState(() {
                          selectedCCEmailInfos.remove(value);
                        });
                      },
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 10,
                          left: 10
                      ),
                      child: Text("BCC",
                          style: TextStyle(
                              fontSize: 17
                          )
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      decoration: kBoxDecorationStyle,
                      child: TextField(
                        controller: bccTextEditCtl,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(top: 15.0),
                          suffixIcon: IconButton(
                            icon: Icon(Icons.add),
                            onPressed: (){
                              bool isVerified = verifyBeforeAddEmail(
                                  context,
                                  bccTextEditCtl.text,
                                  true
                              );
                              if(isVerified){
                                addEmailAddress(2);
                              }
                            },
                          ),
                          prefixIcon: IconButton(
                            icon: Icon(Icons.search),
                            onPressed: (){
                              MySharedPreferences.getStringValue("emailInfos").then((emailInfos){
                                if(emailInfos == null){
                                  showMultiSelect(context,2,[]);
                                }else {
                                  emailInfos.substring(0);
                                  List<EmailInfo> emailInfoList = emailInfos
                                      .split(";")
                                      .map((e) => EmailInfo(emailAddress: e))
                                      .toList();
                                  showMultiSelect(context,2,emailInfoList);
                                }
                              });
                            },
                          )
                        ),
                      ),
                    ),
                    MultiSelectChipDisplay(
                      items: selectedBCCEmailInfos.map((e) => MultiSelectItem(
                          e, e.emailAddress
                      )).toList(),
                      height: 5.0,
                      chipWidth: 50,
                      textStyle: TextStyle(
                        fontSize: 10,
                      ),
                      onTap: (value) {
                        setState(() {
                          selectedBCCEmailInfos.remove(value);
                        });
                      },
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 10,
                          left: 10
                      ),
                      child: Text("SUBJECT",
                          style: TextStyle(
                              fontSize: 17
                          )
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      decoration: kBoxDecorationStyle,
                      child: TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(left: 14.0),
                        ),
                        controller: subjectTextEditCtl,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 10,
                          left: 10
                      ),
                      child: Text("BODY",
                          style: TextStyle(
                              fontSize: 17
                          )
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      decoration: kBoxDecorationStyle,
                      child: HtmlEditor(
                        controller: htmlEditorController, //required
                        hint: "Your text here...",
                        options: HtmlEditorOptions(
                          height: 250,
                          showBottomToolbar: false,
                          autoAdjustHeight: true,
                          adjustHeightForKeyboard: true
                        )
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 10,
                          left: 10
                      ),
                      child: Text("ATTACHMENTS",
                          style: TextStyle(
                              fontSize: 17
                          )
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        top: 10,
                        left: 10,
                        bottom: 10
                      ),
                      child: ListView.builder(
                        padding: EdgeInsets.fromLTRB(20, 0, 10, 0),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: messageBuilder.attachments.length,
                        itemBuilder: (context, index) {
                          return Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  child: Text(
                                    messageBuilder.attachments[index].name,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  width: 200,
                                ),
                                IconButton(
                                    onPressed: (){
                                      setState(() {
                                        messageBuilder.attachments.removeAt(index);
                                      });
                                    },
                                    icon: Icon(Icons.close)
                                )
                              ],
                            ),
                          );
                        }
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
      ),
    );
  }

  void showMultiSelect(BuildContext context,int type,List<EmailInfo> emailInfos) async {
    await showDialog(
      context: context,
      builder: (ctx) {
        return  MultiSelectDialog(
          items: emailInfos.map((emailInfo) => MultiSelectItem<EmailInfo>(
            emailInfo,
            emailInfo.emailAddress
          )).toList(),
          initialValue: type == 1 ? selectedCCEmailInfos : selectedBCCEmailInfos,
          title: Text("Email Address"),
          searchable: false,
          onConfirm: (ces) {
            List<EmailInfo> confirmEmails = ces;
            confirmEmails.forEach((confirmEmail) {
              if(type == 1){
                bool isContain = selectedCCEmailInfos
                .any((selectedCCEmail) =>
                  selectedCCEmail.emailAddress == confirmEmail.emailAddress
                );
                if(!isContain){
                  setState(() {
                    selectedCCEmailInfos.add(EmailInfo(
                        emailAddress: confirmEmail.emailAddress
                    ));
                  });
                }
              }else{
                bool isContain = selectedBCCEmailInfos
                    .any((selectedBCCEmail) =>
                selectedBCCEmail.emailAddress ==confirmEmail.emailAddress);
                if(!isContain){
                  setState(() {
                    selectedBCCEmailInfos.add(EmailInfo(
                        emailAddress: confirmEmail.emailAddress
                    ));
                  });
                }
              }
            });
          },
        );
      },
    );
  }

  void addEmailAddress(int type) {
    String email = type == 1 ? ccTextEditCtl.text : bccTextEditCtl.text;
    if(type == 1){
      setState(() {
        bool needToAdd = isEmailExist(selectedCCEmailInfos,ccTextEditCtl.text);
        if(needToAdd){
          setState(() {
            selectedCCEmailInfos.add(EmailInfo(
                emailAddress: ccTextEditCtl.text
            ));
          });
        }
      });
      ccTextEditCtl.clear();
    }else {
      bool needToAdd = isEmailExist(selectedBCCEmailInfos,bccTextEditCtl.text);
      setState(() {
        if(needToAdd){
          selectedBCCEmailInfos.add(EmailInfo(
              emailAddress: bccTextEditCtl.text
          ));
        }
      });
      bccTextEditCtl.clear();
    }

    MySharedPreferences.getStringValue("emailInfos").then((emailInfos){
      if(emailInfos == null){
        MySharedPreferences.setStringValue("emailInfos","$email");
      }else {
        bool needToStore = true;
        emailInfos.split(";").forEach((emailInfo) {
          if(emailInfo == email){
            needToStore = false;
          }
        });
        if(needToStore){
          MySharedPreferences.setStringValue("emailInfos","$emailInfos;$email");
        }
      }
    });

  }

  Future<void> sendMail(BuildContext buildContext) async {

    setState(() {
      needToFreezeUi = true;
    });

    MySnackBar.show(buildContext, MySnackBar.loadingIcon, "Please wait...!");
    SmtpClient smtpClient = SmtpClient(domain, isLogEnabled: true);

    try {

      await smtpClient.connectToServer(
        "$serverPrefix.${userInfo.domainName}",
        smtpServerPort,
        isSecure: isSmtpServerSecure
      );
      await smtpClient.ehlo();
      await smtpClient.authenticate(userInfo.email, userInfo.password);

      messageBuilder.from = [MailAddress('', userInfo.email)];
      messageBuilder.to = [MailAddress('', toTextEditCtl.text)];
      messageBuilder.cc = selectedCCEmailInfos.map((e) => MailAddress('',e.emailAddress)).toList();
      messageBuilder.bcc = selectedBCCEmailInfos.map((e) => MailAddress('',e.emailAddress)).toList();
      messageBuilder.subject = subjectTextEditCtl.text;
      String htmlText = await htmlEditorController.getText();
      messageBuilder.addTextHtml(htmlText);

      messageBuilder.hasAttachments ? messageBuilder.getPart(
          MediaSubtype.multipartAlternative,
          recursive: false
      ) : messageBuilder.addPart(
          mediaSubtype: MediaSubtype.multipartAlternative,
          insert: true
      );

      if (!messageBuilder.hasAttachments) {
        messageBuilder.setContentType(
            MediaType.fromSubtype(MediaSubtype.multipartAlternative)
        );
      }

      messageBuilder.replyToSimplifyReferences = true;
      MimeMessage mimeMessage = messageBuilder.buildMimeMessage();
      SmtpResponse smtpResponse = await smtpClient.sendMessage(mimeMessage);

      MySnackBar.hide(buildContext);
      if(smtpResponse.isOkStatus){
        MySnackBar.show(buildContext,MySnackBar.successIcon,"Mail send successfully");
        checkUserExistOnFireStore(toTextEditCtl.text,subjectTextEditCtl.text);
        clearInputFields(buildContext);
      }else {
        MySnackBar.show(buildContext,MySnackBar.errorIcon,"Something went wrong, please try again!");
      }

    } catch (e) {
      MySnackBar.show(buildContext,MySnackBar.errorIcon,"Something went wrong, please try again!");
    }
    setState(() {
      needToFreezeUi = false;
    });
  }

  bool isEmailExist(List<EmailInfo> emailInfos, String text) {
    bool needToAdd = true;
    emailInfos.forEach((element) {
      if(element.emailAddress == text){
        needToAdd = false;
      }
    });
    return needToAdd;
  }

  Future<bool> verifyInput(BuildContext buildContext) async {
    bool isInputVerified = true;
    String htmlText = await htmlEditorController.getText();

    if (toTextEditCtl.text.isEmpty) {
      Alert.show(alertDialog, buildContext, Alert.ERROR, "To whom you want send mail is required!");
      isInputVerified = false;
    } else if (!emailRegExp.hasMatch(toTextEditCtl.text)) {
      Alert.show(alertDialog, buildContext, Alert.ERROR,
          "Email address format not correct!");
      isInputVerified = false;
    } else if (subjectTextEditCtl.text.isEmpty) {
      Alert.show(alertDialog, buildContext, Alert.ERROR, "Subject required!");
      isInputVerified = false;
    } else if (htmlText.isEmpty) {
      Alert.show(alertDialog,buildContext,Alert.ERROR,"Body Required!");
      isInputVerified = false;
    }

    bool isCcVerified = verifyBeforeAddEmail(
      buildContext,
      ccTextEditCtl.text,
      false
    );

    if(isCcVerified){
      selectedCCEmailInfos.add(
        EmailInfo(emailAddress: ccTextEditCtl.text)
      );
    }

    bool isBccVerified = verifyBeforeAddEmail(
      buildContext,
      bccTextEditCtl.text,
      false
    );

    if(isBccVerified){
      selectedCCEmailInfos.add(
        EmailInfo(emailAddress: bccTextEditCtl.text)
      );
    }

    return isInputVerified;
  }

  bool verifyBeforeAddEmail(BuildContext buildContext, String email, bool needToShowAlert) {

    bool isInputVerified = true;
    String msg;

    if (email.isEmpty) {
      msg = "No email to add!";
      isInputVerified = false;
    } else if (!emailRegExp.hasMatch(email)) {
      msg = "Email address format not correct!";
      isInputVerified = false;
    }

    if(!isInputVerified && needToShowAlert){
      Alert.show(alertDialog, buildContext, Alert.ERROR, msg);
    }

    return isInputVerified;
  }

  clearInputFields(BuildContext context){
    setState(() {
      toTextEditCtl.clear();
      selectedCCEmailInfos.clear();
      selectedBCCEmailInfos.clear();
      subjectTextEditCtl.clear();
      htmlEditorController.clear();
      messageBuilder.attachments.clear();
    });
  }

  Future<bool> onFileSelect(BuildContext context) async {

    final result = await FilePicker.platform
        .pickFiles(type: FileType.any, allowMultiple: true, withData: true);
    if (result == null) {
      return false;
    }
    for (final file in result.files) {
      final lastDotIndex = file.path.lastIndexOf('.');
      MediaType mediaType;
      if (lastDotIndex == -1 || lastDotIndex == file.path.length - 1) {
        mediaType = MediaType.fromSubtype(MediaSubtype.applicationOctetStream);
      } else {

        final ext = file.path.substring(lastDotIndex + 1);
        mediaType = MediaType.guessFromFileExtension(ext);
      }
      setState(() {
        messageBuilder.addBinary(file.bytes, mediaType, filename: file.name);
      });
    }
    return true;

  }


  void checkUserExistOnFireStore(String to, String subject) {

    FirebaseFirestore.instance
        .collection('userInfos')
        .doc(userInfo.email)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        makeAnEntryNotificationCollection(documentSnapshot, to, subject);
        print('Document exists on the database');
      }else {
        print('Document not exists on the database');
      }
    }).catchError((err){
      print("err $err");
    });

  }

  void makeAnEntryNotificationCollection(
      DocumentSnapshot documentSnapshot,
      String to,
      String subject
      ) {
    CollectionReference notifications = FirebaseFirestore.instance.collection('notifications');
    notifications.add({
      "title" : "NetxMail from $to",
      "body" : subject,
      "fcmToken" : documentSnapshot.get("fcmToken"),
      "email" : documentSnapshot.get("email")
    }).then((res){
      print("Notification entry successful!");
    }).catchError((err){
      print("Notification entry unsuccessful!");
    });
  }

}