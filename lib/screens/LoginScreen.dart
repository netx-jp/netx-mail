import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enough_mail/imap/imap_client.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:netxmail/constants.dart';
import 'package:netxmail/models/Domain.dart';
import 'package:netxmail/models/UserInfo.dart';
import 'package:netxmail/screens/MainScreen.dart';
import 'package:netxmail/utilities/MySharedPreferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {

  TextEditingController emailCtl = new TextEditingController();
  TextEditingController passwordCtl = new TextEditingController();

  List<Domain> domains = [
    Domain(id: 1, domainName: "netx.jp"),
    Domain(id: 2, domainName: "dreamhalalfood.jp")
  ];

  Domain selectedDomain;

  @override
  void initState() {
    super.initState();
    selectedDomain = domains[0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.white24,
                      Colors.white38,
                      Colors.white54,
                      Colors.white60,
                    ],
                    stops: [0.1, 0.4, 0.7, 0.9],
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 40.0,
                    vertical: 120.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Image.memory(
                          base64.decode(logoBase64String)
                        )
                      ),
                      SizedBox(height: 50.0),
                      buildDomainSelectionTF(),
                      SizedBox(height: 30.0),
                      buildEmailTF(context),
                      SizedBox(
                        height: 30.0,
                      ),
                      buildPasswordTF(context),
                      buildLoginBtn(context)
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildDomainSelectionTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Domain',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          padding: EdgeInsets.only(left: 10),
          child: DropdownButton<Domain>(
            dropdownColor: Colors.grey.shade300,
            focusColor:Colors.grey,
            value: selectedDomain,
            elevation: 5,
            isExpanded: true,
            underline: Container(
              color: Colors.transparent,
            ),
            iconEnabledColor:Colors.grey,
            style: TextStyle(
              color: Colors.red,
              fontFamily: 'OpenSans',
            ),
            items: domains.map<DropdownMenuItem<Domain>>((Domain value) {
              return DropdownMenuItem<Domain>(
                value: value,
                child: Text(
                  value.domainName,
                  style:TextStyle(
                      color: Colors.grey,
                      fontFamily: 'OpenSans'
                  ),
                ),
              );
            }).toList(),
            onChanged: (Domain d) {
              setState(() {
                selectedDomain = d;
              });
            },
          ),
        )
      ],
    );
  }

  Widget buildEmailTF(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: emailCtl,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.grey,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.grey,
              ),
              hintText: 'Enter your Email',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget buildPasswordTF(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: passwordCtl,
            obscureText: true,
            style: TextStyle(
              color: Colors.grey,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.grey,
              ),
              hintText: 'Enter your Password',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget buildLoginBtn(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: ElevatedButton(
        style: TextButton.styleFrom(
          minimumSize: Size(double.infinity, 50),
          padding: EdgeInsets.symmetric(
            vertical: kDefaultPadding,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          backgroundColor: Colors.redAccent,
          elevation: 1.0,
        ),
        onPressed: () async {

          bool isInputVerified = verifyInput(context);

          if(isInputVerified){

            showLoadingBar(context);

            ImapClient client = ImapClient(isLogEnabled: false);
            await client.connectToServer(
              "$serverPrefix.${selectedDomain.domainName}",
              imapServerPort,
              isSecure: isImapServerSecure
            );

            String fcmToken = await FirebaseMessaging.instance.getToken();

            var ui = {
              "domainId" : selectedDomain.id,
              "domainName" : selectedDomain.domainName,
              "email" : emailCtl.text,
              "password" : passwordCtl.text,
              "fcmToken" : fcmToken
            };

            saveUserInfoToFireStore(ui);

            client.login(emailCtl.text, passwordCtl.text).then((value){
              MySharedPreferences.setStringValue("userInfo",
                jsonEncode(ui)
              ).whenComplete((){
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => MainScreen(
                    userInfo: UserInfo(
                      domainId: selectedDomain.id,
                      domainName: selectedDomain.domainName,
                      email: emailCtl.text,
                      password: passwordCtl.text,
                      fcmToken: fcmToken
                    ),
                  )),(route) => false
                );
              });
            }).catchError((e){
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Row(
                  children: [
                    SizedBox(
                      height: 20,
                      width: 20,
                      child: Icon(
                        Icons.error,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(width: 20),
                    Text("Incorrect email and password!",
                      style: TextStyle(
                          color: Colors.white
                      ),
                    )
                  ],
                ),
                backgroundColor: Colors.redAccent,
                duration: Duration(seconds: 5),
              ));
            });
          }
        },
        child: Text(
          'LOGIN',
          style: TextStyle(
            color: Colors.white,
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  bool verifyInput(BuildContext context) {

    bool isInputVerified = true;

    if(emailCtl.text.isEmpty){
      isInputVerified = false;
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Email must not be empty!",
          style: TextStyle(
            color: Colors.white
          ),
        ),
        backgroundColor: Colors.red,
      ));
    } else if(passwordCtl.text.isEmpty){
      isInputVerified = false;
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Password must not be empty!",
          style: TextStyle(
              color: Colors.white
          ),
        ),
        backgroundColor: Colors.red,
      ));
    }

    return isInputVerified;
  }

  void saveUserInfoToFireStore(Map<String, Object> userInfo) {
    CollectionReference userInfos = FirebaseFirestore.instance.collection('userInfos');
    userInfos.doc(userInfo["email"]).set(userInfo)
        .then((value){
      print("User information saved in firestore!");
    }).catchError((err){
      print("User information not saved in firestore!");
    });
  }

  void showLoadingBar(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Row(
        children: [
          SizedBox(
            height: 20,
            width: 20,
            child: CircularProgressIndicator(
              strokeWidth: 2,
            ),
          ),
          SizedBox(width: 20),
          Text("Please wait....!",
            style: TextStyle(
                color: Colors.white
            ),
          )
        ],
      ),
      backgroundColor: Colors.blueAccent,
      duration: Duration(minutes: 5),
    ));
  }
}