import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/material.dart';
import 'package:netxmail/components/ListOfEmails.dart';
import 'package:netxmail/components/SideMenu.dart';
import 'package:netxmail/models/UserInfo.dart';
import 'package:netxmail/responsive.dart';
import 'package:netxmail/screens/EmailScreen.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key, this.userInfo}) : super(key: key);

  final UserInfo userInfo;

  @override
  MainScreenState createState() => MainScreenState(userInfo: userInfo);
}

class MainScreenState extends State<MainScreen> {

  UserInfo userInfo;
  MainScreenState({Key key,this.userInfo});
  MimeMessage mimeMessage;
  GlobalKey<EmailScreenState> emailScreenStateKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    mimeMessage = null;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Responsive(
          mobile: ListOfEmails(userInfo: userInfo),
          tablet: Row(
            children: [
              Expanded(
                flex: 6,
                child: ListOfEmails(
                  userInfo: userInfo,
                  onMimeMessageTap: (MimeMessage mm){
                    emailScreenStateKey.currentState.updateMimeMessage(mm);
                  },
                ),
              ),
              Expanded(
                flex: 9,
                child: EmailScreen(
                  key: emailScreenStateKey,
                  userInfo: userInfo,
                  mimeMessage : mimeMessage
                ),
              ),
            ],
          ),
          desktop: Container(),
        )
      )
    );
  }
}
