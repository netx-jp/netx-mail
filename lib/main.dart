import 'dart:async';
import 'dart:convert';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:netxmail/constants.dart';
import 'package:netxmail/models/UserInfo.dart';
import 'package:netxmail/screens/LoginScreen.dart';
import 'package:netxmail/screens/MainScreen.dart';
import 'package:netxmail/utilities/FadeAnimation.dart';
import 'package:shared_preferences/shared_preferences.dart';

AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
  playSound: true
);

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'NetxMail',
      theme: ThemeData(),
      home: WelcomePage(title: "Welcome To NetxMail"),
    );
  }
}

class WelcomePage extends StatefulWidget {
  WelcomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  WelcomePageState createState() => WelcomePageState();
}

class WelcomePageState extends State<WelcomePage>
    with SingleTickerProviderStateMixin {

  AnimationController animationController;
  Future<SharedPreferences> sharedPreferences = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              color: Colors.blue,
              playSound: true,
              icon: '@mipmap/ic_launcher',
            ),
          )
        );
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        showDialog(
          context: context,
          builder: (_) {
            return AlertDialog(
              title: Text(notification.title),
              content: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Text(notification.body)],
                ),
              ),
            );
          }
        );
      }
    });

    Timer(Duration(seconds: 5), () {
      sharedPreferences.then((value){
        var userInfo = value.getString("userInfo");
        if(userInfo == null){
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) =>
              LoginScreen()
            ), (route) => false
          );
        }else {
          var userInfoObj = jsonDecode(userInfo);
          Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) => MainScreen(
              userInfo: UserInfo(
                domainId: userInfoObj['domainId'],
                domainName: userInfoObj['domainName'],
                email: userInfoObj['email'],
                password: userInfoObj['password'],
                fcmToken: userInfoObj['fcmToken']
              ),
            )), (route) => false
          );
        }
      }).catchError((err){
        print("error = $err");
      });
    });

    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300)
    );

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  FadeAnimation(
                    3,
                    Container(
                      height: 150,
                      width: 150,
                      padding: EdgeInsets.all(5),
                      child: Image.memory(base64.decode(welcomeLogoBase64String))
                    ),
                  ),
                  SizedBox(
                    height: 20
                  ),
                  FadeAnimation(
                    4,
                    Text(
                      "Welcome to NetxMail",
                      style: TextStyle(
                        fontSize: 20
                      ),
                    )
                  ),
                  SizedBox(
                      height: 5
                  ),
                  FadeAnimation(5,Text("App Version: $appVersion"))
                ],
              ),
            ),
          ),
        ),
      )
    );
  }

}
