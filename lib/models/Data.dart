import 'package:enough_mail/message_builder.dart';

class Data{
  MessageBuilder messageBuilder;
  bool isNewMessage;
  String email;
  Data({
    this.messageBuilder,
    this.isNewMessage,
    this.email
  });
}
