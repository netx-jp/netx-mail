class UserInfo {
  int domainId;
  String domainName;
  String email;
  String password;
  String fcmToken;
  UserInfo({
    this.domainId,
    this.domainName,
    this.email,
    this.password,
    this.fcmToken
  });
}
