import 'package:flutter/material.dart';

class Domain {

  int id;
  String domainName;

  Domain({
    this.id,
    this.domainName
  });
}