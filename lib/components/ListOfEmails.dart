import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:netxmail/components/EmailCard.dart';
import 'package:netxmail/components/SideMenu.dart';
import 'package:netxmail/constants.dart';
import 'package:netxmail/models/Email.dart';
import 'package:netxmail/models/UserInfo.dart';
import 'package:netxmail/responsive.dart';
import 'package:netxmail/screens/EmailScreen.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class ListOfEmails extends StatefulWidget {
  ListOfEmails({Key key, this.userInfo, this.onMimeMessageTap}) : super(key: key);

  final void Function(MimeMessage) onMimeMessageTap;
  final UserInfo userInfo;

  @override
  ListOfEmailsState createState() => ListOfEmailsState(
      userInfo: userInfo,
    onMimeMessageTap: onMimeMessageTap
  );
}

class ListOfEmailsState extends State<ListOfEmails> {

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  Future<List<MimeMessage>> mimeMessages;

  UserInfo userInfo;
  Function(MimeMessage) onMimeMessageTap;
  ListOfEmailsState({Key key,this.userInfo, this.onMimeMessageTap});

  String msgPath = "Inbox";

  @override
  void initState() {
    super.initState();
    msgPath = "Inbox";
    mimeMessages = getMimeMessage(msgPath);
    print("ListOfEmails user info ${userInfo.email}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 250),
        child: SideMenu(
          userInfo: userInfo,
          onSideMenuTap: (String messagePath){
            print("path = $messagePath");
            setState(() {
              msgPath = messagePath;
              mimeMessages = getMimeMessage(messagePath);
            });
            scaffoldKey.currentState.openEndDrawer();
          },
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.white24,
              Colors.white38,
              Colors.white54,
              Colors.white60,
            ],
            stops: [0.1, 0.4, 0.7, 0.9],
          ),
        ),
        child: SafeArea(
          right: false,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                height: kDefaultToolbarHeight,
                color: Colors.redAccent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      icon: Icon(Icons.menu),
                      color: Colors.white,
                      onPressed: () async {
                        scaffoldKey.currentState.openDrawer();
                      },
                    ),
                    Expanded(
                      child: Text(
                        msgPath.toUpperCase(),
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: FutureBuilder(
                  future: mimeMessages,
                  builder: (context, snapshot){
                    if(snapshot.connectionState == ConnectionState.waiting){
                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }else{
                      if(snapshot.hasData){
                        return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            MimeMessage message = snapshot.data[index];
                            return EmailCard(
                              isActive: false,
                              email: Email(
                                name: message.fromEmail,
                                body: message.decodeTextPlainPart() == null ?
                                "Empty Body" : message.decodeTextPlainPart(),
                                isChecked: true,
                                isAttachmentAvailable: message.hasAttachments(),
                                subject: message.decodeSubject(),
                                time: DateFormat().format(DateTime.parse(message.decodeDate().toLocal().toString())),
                                image: "assets/images/user_1.png",
                                emailIcon: Icon(Icons.email)
                              ),
                              press: () {
                                if(Responsive.isMobile(context)){
                                  Navigator.push(context,MaterialPageRoute(
                                    builder: (context) =>
                                    EmailScreen(
                                      userInfo: userInfo,
                                      mimeMessage: message
                                    )
                                  ));
                                }else {
                                  this.onMimeMessageTap(message);
                                }
                              },
                            );
                          },
                        );
                      }else {
                        return Container(
                          child: Center(
                            child: Text("No message found!",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                                color: Colors.red
                              ),
                            ),
                          ),
                        );
                      }
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<List<MimeMessage>> getMimeMessage(String messagePath) async {

    ImapClient imapClient = ImapClient(isLogEnabled: false);
    await imapClient.connectToServer(
      "$serverPrefix.${userInfo.domainName}",
      imapServerPort,
      isSecure: isImapServerSecure
    );

    // The path for these folders are:
    // Inbox: Inbox
    // Drafts Path: Drafts
    // Junk Path: Junk
    // Deleted Items Path: Trash
    // Sent Items Path: Sent



    await imapClient.login(userInfo.email, userInfo.password);
    await imapClient.listMailboxes();
    await imapClient.selectMailboxByPath(messagePath);
    FetchImapResult fetchImapResult = await imapClient
        .fetchRecentMessages(criteria: 'BODY.PEEK[]');

    return fetchImapResult.messages.reversed.toList();
  }

}

