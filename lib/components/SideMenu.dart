import 'dart:convert';
import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:netxmail/components/SideMenuItem.dart';
import 'package:netxmail/models/Data.dart';
import 'package:netxmail/models/UserInfo.dart';
import 'package:netxmail/responsive.dart';
import 'package:netxmail/screens/LoginScreen.dart';
import 'package:netxmail/screens/MailSender.dart';
import 'package:netxmail/utilities/MySharedPreferences.dart';
import 'package:websafe_svg/websafe_svg.dart';
import 'package:netxmail/constants.dart';

class SideMenu extends StatefulWidget {

  final void Function(String) onSideMenuTap;
  final UserInfo userInfo;

  SideMenu({
    Key key,
    this.onSideMenuTap,
    this.userInfo
  }) : super(key: key);

  @override
  SideMenuState createState() => SideMenuState(
    userInfo: userInfo,
    onSideMenuTap: onSideMenuTap
  );
}

class SideMenuState extends State<SideMenu> {

  UserInfo userInfo;
  Function(String) onSideMenuTap;
  SideMenuState({Key key,this.userInfo, this.onSideMenuTap});

  var sideMenuState = [
    {
      "isActive": false,
      "itemCount": 0
    },
    {
      "isActive": false,
      "itemCount": 0
    },
    {
      "isActive": false,
      "itemCount": 0
    }
  ];


  @override
  void initState() {
    super.initState();
    print("userInfo = ${userInfo.email}");
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    if(mediaQueryData.orientation == Orientation.landscape){
      return SingleChildScrollView(
        child: getSideNavMenuItems()
      );
    }else {
      return getSideNavMenuItems();
    }
  }

  Widget getSideNavMenuItems(){
    return Container(
      color: kBgLightColor,
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
              child: SingleChildScrollView(
                child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                          child: Column(
                            children: [
                              SizedBox(height: kDefaultPadding),
                              Row(
                                children: [
                                  Image.memory(
                                    base64.decode(logoBase64String),
                                    width: 100,
                                  ),
                                  Spacer(),
                                  if (!Responsive.isDesktop(context)) CloseButton(),
                                ],
                              ),
                              SizedBox(height: kDefaultPadding),
                              TextButton.icon(
                                style: TextButton.styleFrom(
                                    minimumSize: Size(double.infinity, 50),
                                    padding: EdgeInsets.symmetric(
                                      vertical: kDefaultPadding,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    backgroundColor: kPrimaryColor,
                                    shadowColor: Color(0xFF234395).withOpacity(0.2)
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => MailSender(
                                        userInfo: userInfo,
                                        data: Data(
                                            messageBuilder: MessageBuilder(),
                                            isNewMessage: true,
                                            email: userInfo.email
                                        ),
                                      ))
                                  );
                                },
                                icon: WebsafeSvg.asset("assets/Icons/Edit.svg", width: 16),
                                label: Text(
                                  "New message",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              SizedBox(height: kDefaultPadding),
                              SideMenuItem(
                                  press: () {
                                    setState(() {
                                      sideMenuState.forEach((element) {
                                        element['isActive'] = false;
                                        print("el ${element['isActive']}");
                                      });
                                      sideMenuState[0]['isActive'] = true;
                                    });
                                    this.onSideMenuTap("Inbox");
                                  },
                                  title: "Inbox",
                                  iconSrc: "assets/Icons/Inbox.svg",
                                  isActive: sideMenuState[0]["isActive"],
                                  itemCount: sideMenuState[0]["itemCount"]
                              ),
                              SideMenuItem(
                                  press: () {
                                    setState(() {
                                      sideMenuState.forEach((element) {
                                        element['isActive'] = false;
                                      });
                                      sideMenuState[1]['isActive'] = true;
                                    });
                                    this.onSideMenuTap("Sent");
                                  },
                                  title: "Sent",
                                  iconSrc: "assets/Icons/Send.svg",
                                  isActive: sideMenuState[1]["isActive"],
                                  itemCount: sideMenuState[1]["itemCount"]
                              ),
                              SideMenuItem(
                                  press: () {
                                    setState(() {
                                      sideMenuState.forEach((element) {
                                        element['isActive'] = false;
                                      });
                                      sideMenuState[2]['isActive'] = true;
                                    });
                                    this.onSideMenuTap("Junk");
                                  },
                                  title: "Junk",
                                  iconSrc: "assets/Icons/File.svg",
                                  isActive: sideMenuState[2]["isActive"],
                                  itemCount: sideMenuState[2]["itemCount"]
                              ),
                              SizedBox(height: kDefaultPadding * 2),
                              TextButton.icon(
                                style: TextButton.styleFrom(
                                  minimumSize: Size(double.infinity, 50),
                                  padding: EdgeInsets.symmetric(
                                    vertical: kDefaultPadding,
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  backgroundColor: Colors.red,
                                ),
                                onPressed: () {
                                  MySharedPreferences.clear("userInfo").whenComplete((){
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => LoginScreen()
                                        ),(route) => false
                                    );
                                  });
                                },
                                icon: Icon(Icons.logout,
                                  color: Colors.white,
                                ),
                                label: Text(
                                  "Logout",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              SizedBox(height: kDefaultPadding * 2)
                            ],
                          ),
                        )
                      ],
                    )
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 20),
              child: Column(
                children: [
                  Text("A PRODUCT OF NETX"),
                  Text("APP VERSION: $appVersion"),
                  Text("SUPPORT: $supportPhoneNumber"),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}
